<?php

namespace App\Entity;

use App\Repository\UsersRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


//     *@UniqueEntity(
//     *field :{"email"},
//     *message="L'email que vous avez indiqué est déjà utilisé !")
   
/**
 * @ORM\Entity(repositoryClass=UsersRepository::class)
 */
class Users 
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;
    
    //  @Assert\EqualTo(propertyPath="password", mmessage="Vous n'avez pas tapé le même mot de passe")

    public $confirm_password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;
    /**
     * @ORM\Column(type="string", length=255)
     */


    // @Assert\Lenght(min="8", minMessage="Votre mot de passe doit contenir minimum 8 caractères")
    // @Assert\EqualTo(propertyPath="confirm_password", mmessage="Vous n'avez pas tapé le même mot de passe")

    private $password;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }


    public function getconfirm_password()
    {
        return $this->confirm_password;
    }

    public function setconfirm_password(string $confirm_password)
    {
        $this->confirm_password = $confirm_password;

        return $this;
    }


      public function eraseCredentials() {
           // eraseCredentials
      }

      public function getSalt() {
          //getSalt
      }

    public function getRoles(){
        return ['ROLES_USER'];
    }
}

<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\InscriptionType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



class SecurityController extends BlogController
{
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        $users = new Users();
        $form =$this->createForm(InscriptionType::class, $users);

        $form->handleRequest($request);
        dump($users);
    

        if($form->isSubmitted() && $form->isValid()){

            $hash= $encoder->encodePassword($users, $users->getPassword());
            $users->setPassword($hash);
            
            $manager->persist($users);
            $manager->flush();
            
            return $this->Response('Inscription validée ! BRAVO');
        }

        return $this->render('security/signup.html.twig',[
            'formUser'=>$form->createView()
        ]);

    }


    
    

    /**
     * @Route ("/deconnexion", name="security_logout")
     */
    public function logout(){
        //deconnexion
    }
}
